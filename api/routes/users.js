const express = require('express'), router = express.Router();
const usersController = require('../controllers/users')


router.get("/users/:id", usersController.getUserById);
router.get("/users/email/:email", usersController.getUserByEmail);
router.post("/users/register", usersController.register);
router.post("/users/login", usersController.login);
router.post("/verify/token",usersController.verifyToken);


module.exports = router;