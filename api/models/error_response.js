class ErrorResponse {
    constructor() {
        this.validationErrors = [];
        this.fault = faults.NOT_FOUND;
    }

    initializeWithFault(fault){
        this.fault = fault;
        return this;
    }
}

const faults = Object.freeze({
    NOT_FOUND: "NOT_FOUND",
    DUPLICATE: "DUPLICATE",
    VALIDATION: "VALIDATION",
    UNAUTHORIZED: "UNAUTHORIZED",
    CRITICAL_SRV_ERROR: "INTERNAL"
});

module.exports = { ErrorResponse, faults }