module.exports.UserModel = class UserModel {
    constructor() {
        this.firstName = "";
        this.lastName = "";
        this.identity = "";
        this.email = "";
        this.registrationDate = new Date();
    }
}