const usersRepository = require('../../storage/users_repository');
const { ErrorResponse, faults } = require('../models/error_response');
const sha1 = require('js-sha1');
const uuidv1 = require('uuid/v1');

module.exports.getUserById = function (req, res, next) {
    usersRepository.get_user_id(req.params.id, (data) => {
        if (typeof data === 'undefined') {
            notFoundWithContext(res, 'id');
        } else {
            res.json(data);
        }
    })
    next();
};

module.exports.getUserByEmail = function (req, res, next) {
    usersRepository.get_user_email(req.params.email, (data) => {
        if (typeof data === 'undefined') {
            notFoundWithContext(res, 'email');
        } else {
            res.json(data[0]);
        }
    }, undefined, next);
};

module.exports.register = function (req, res, next) {
    usersRepository.get_user_email(req.body.email, (data) => {
        if (typeof data === 'undefined') {
            if (typeof req.body.password === 'undefined' || req.body.password == '') {
                let err = new ErrorResponse();
                err.validationErrors.push({ context: 'password', message: 'required field missing' });
                err.fault = faults.VALIDATION;
                res.status(400);
                res.json(err);
                next();
            }
            req.body.hash = sha1(req.body.password);
            usersRepository.create(req.body, (data) => {
                res.status(204);
                res.json();
                next();
            });
        } else {
            let err = new ErrorResponse();
            err.validationErrors.push({ context: 'email', message: 'user already exist' });
            err.fault = faults.DUPLICATE;
            res.status(400);
            res.json(err);
            next();
        }
    })

};

module.exports.login = function (req, res, next) {
    usersRepository.get_user_email(req.body.email, (data) => {
        if (typeof data === 'undefined') {
            notFoundWithContext(res, 'email');
            next();
            return;
        }

        usersRepository.verifyCredentials(
            req.body.email,
            sha1(req.body.password),
            () => {
                const sessionId = uuidv1();
                usersRepository.createSession(data[0], sessionId, {});
                res.json(sessionId);
            },
            () => { res.status(403); res.json(new ErrorResponse().initializeWithFault(faults.UNAUTHORIZED)); }
        )

    }, (DBuser) => {
        return DBuser.user_id;
    }, next)
};

module.exports.verifyToken = function (req, res, next) {
    usersRepository.get_user_by_session_id(req.body.token,
        (data) => {
            res.status(200);
            res.json({
                userId: data.public_id,
                firstName: data.first_name,
                lastName: data.last_name,
                email: data.email,
                lastAction: data.last_action,
                registerDate: data.register_date
            }); 
            next();
        },
        (data) => {
            res.status(400);
            res.json(new ErrorResponse().initializeWithFault(faults.UNAUTHORIZED));
            next();
        });

}


const isUnauthorized = (headers, res) => {
    console.log('Token: ' + headers.authorization);
    if (headers.authorization == 'TOP_SECRET') {
        return false;
    } else {
        res.status(403);
        res.json('autentykacja! FAILED');
        return true;
    }
}


const notFoundWithContext = (res, context) => {
    let err = new ErrorResponse();
    err.validationErrors.push({ context: context });
    err.fault = faults.NOT_FOUND;
    res.status(404);
    res.json(err);
}