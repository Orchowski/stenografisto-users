#!/bin/bash
export DB_PORT=5432
export DB_PASSWORD=postgres
export DB_NAME=stenografisto
export DB_HOST=localhost
export DB_USER=postgres