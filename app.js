const express = require('express');
const app = express();
const cors = require('cors')
const bodyParser = require('body-parser');
const port = 3000;
const { ErrorResponse, faults } = require('./api/models/error_response');

//Auth

//\Auth
app.use(cors())
app.use(express.static('public'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(require('./api/routes/users'));
app.use(function (err, req, res, next) {  
  console.error(err);
  res.status(500);
  res.json(new ErrorResponse().initializeWithFault(faults.CRITICAL_SRV_ERROR));
})


app.listen(port, () => {
  console.log('Aleksander Orchowski - stenografisto-users...')
  console.log(`App listening on port ${port}!`);
});