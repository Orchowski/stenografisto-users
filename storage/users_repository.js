const dbContext = require('./db_context');
const { UserModel } = require('../api/models/user');
const uuidv1 = require('uuid/v1');

module.exports.get_user_id = function (requestedId, onDataAchieved) {
    dbContext.execute(
        async function (client) {
            const res = (await client.query('SELECT * FROM users WHERE public_id = $1', [requestedId]))
                .rows
                .map(x => {
                    let user = new UserModel();
                    user.firstName = x.first_name;
                    user.lastName = x.last_name;
                    user.identity = x.public_id;
                    user.email = x.email;
                    user.registrationDate = x.register_date;
                    return user;
                });
            if (res === undefined || res.length == 0) {
                onDataAchieved(undefined);
                return;
            }
            onDataAchieved(res[0]);
        }
    );
}



module.exports.get_user_email = function (requestedEmail, onDataAchieved, map, onFailure) {
    dbContext.execute(
        async function (client) {
            const res = (await client.query('SELECT * FROM users WHERE email = $1', [requestedEmail]))
                .rows
                .map(x => {
                    if (typeof map != 'undefined') {
                        return map(x);
                    }
                    let user = new UserModel();
                    user.firstName = x.first_name;
                    user.lastName = x.last_name;
                    user.identity = x.public_id;
                    user.email = x.email;
                    user.registrationDate = x.register_date;
                    return user;
                });
            if (res === undefined || res.length == 0) {
                onDataAchieved(undefined);
                return;
            }
            onDataAchieved(res);
        }
    ).catch(onFailure);
}


module.exports.create = function (userBody, onDataAchieved) {
    dbContext.execute(
        //TODO : wrap into transaction
        async function (client) {
            const generatedID = uuidv1();
            const createDate = Date.now();
            var res = (await client.query('insert into users (public_id, first_name, last_name, email, register_date) values ($1, $2, $3, $4, (to_timestamp( $5 / 1000.0)))',
                [generatedID, userBody.firstName, userBody.lastName, userBody.email, createDate]));
            if (res.rowCount == '1') {
                var userID = (await client.query('select user_id from users where public_id = $1', [generatedID])).rows[0].user_id;
                res = (await client.query('insert into credentials (user_id, hash, set_date) values ($1, $2, (to_timestamp( $3 / 1000.0)))',
                    [userID, userBody.hash, createDate]));
            }
            onDataAchieved(res);
        }
    );
}

module.exports.verifyCredentials = function (email, hash, onSuccess, onFailure) {
    dbContext.execute(
        async function (client) {
            try {
                var userID = (await client.query('select user_id from users where email = $1', [email])).rows[0].user_id;
                var isPasswordValid = (await client.query('select hash from credentials where user_id = $1', [userID])).rows[0].hash == hash;
                if (isPasswordValid) { onSuccess(); }
                else { onFailure(); }
            } catch (e) { onFailure(); }
        }
    );
}

module.exports.createSession = function (userID, sessionId, data) {
    dbContext.execute(
        async function (client) {
            await client.query('insert into sessions (user_id, public_id, data, last_action) values ($1, $2, $3, (to_timestamp( $4 / 1000.0)))',
                [userID, sessionId, data, Date.now()]);
        }
    );
}

module.exports.get_user_by_session_id = function (sessionId, onSuccess, onFailure) {
    dbContext.execute(
        async function (client) {
            var session = (await client.query('select * from sessions as s left join users as u on s.user_id = u.user_id where s.public_id = $1', [sessionId])).rows[0];
            if (session) {
                client.query('update sessions set last_action = (to_timestamp( $2 / 1000.0)) where session_id = $1', [session.session_id, Date.now()])
                onSuccess(session);
            } else {
                onFailure();
            }
        }
    );
}