const { Pool } = require('pg');
const pg = require('pg');
const tasks = require('./tasks');
const configs = require('../config')
const { moment } = require('moment')
pg.types.setTypeParser(1114, str => moment.utc(str).format());

const client = new Pool(configs.database);


(async function () {
    await client.connect();
})()

module.exports.execute = async function execute(dbRequest) {
    await client.query('SET search_path TO users');    
    await dbRequest(client);
};