const dbContext = require('./db_context');
const cron = require('node-cron');

cron.schedule('*/10 * * * *', async () => {
    dbContext.execute(async (client)=>{
        console.log('clear old sessions performed');
        await client.query('select clear_old_sessions()');
    });
  
}).start();


