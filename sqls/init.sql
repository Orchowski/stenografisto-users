CREATE TABLE users (
    user_id serial,
    public_id varchar(38) NOT NULL,
    first_name varchar(30) NOT NULL,
    last_name varchar(30) NOT NULL,
    email varchar(40) NOT NULL,
    register_date timestamptz NOT NULL,
    CONSTRAINT PK_User PRIMARY KEY (user_id)
);

CREATE TABLE credentials (
    id serial,
    user_id INT NOT NULL,
    hash varchar(200) NOT NULL,    
    set_date timestamptz NOT NULL,
    CONSTRAINT PK_Credentials PRIMARY KEY (id),
    CONSTRAINT FK_users_credential FOREIGN KEY (user_id) REFERENCES users(user_id)
);

CREATE TABLE sessions (
    user_id INT NULL,
    session_id serial,
    public_id varchar(38) NOT NULL,
    data jsonb NOT NULL,
    last_action timestamptz NOT NULL,
    CONSTRAINT PK_Session PRIMARY KEY (session_id),
    CONSTRAINT FK_users_session FOREIGN KEY (user_id) REFERENCES users(user_id)
);


CREATE FUNCTION clear_old_sessions ()    
    RETURNS void
	LANGUAGE plpgsql
AS $$
BEGIN
    DELETE FROM sessions
    WHERE last_action < NOW() - INTERVAL '10 minute';    
END;
$$;